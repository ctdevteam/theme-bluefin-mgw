import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import {AccordionModule} from 'primeng/accordion';     //accordion and accordion tab
import {MenuItem} from 'primeng/api';                 //api
import {BrowserAnimationsModule,NoopAnimationsModule} from '@angular/platform-browser/animations';
import {ToolbarModule} from 'primeng/toolbar';
import {SplitButtonModule} from 'primeng/splitbutton';
import { AisProfileComponent } from './ais-profile/ais-profile.component';
import { RoleComponent } from './role/role.component';
import { UserComponent } from './user/user.component';
import { CustomerComponent } from './customer/customer.component';
import {TableModule} from 'primeng/table';
@NgModule({
  declarations: [
    AppComponent,
    AisProfileComponent,
    RoleComponent,
    UserComponent,
    CustomerComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AccordionModule,
    BrowserAnimationsModule,
    NoopAnimationsModule,
    ToolbarModule,
    SplitButtonModule,
    TableModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
