import { UserComponent } from './user/user.component';
import { RoleComponent } from './role/role.component';
import { CustomerComponent } from './customer/customer.component';
import { AisProfileComponent } from './ais-profile/ais-profile.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: '*', redirectTo: '/user', pathMatch: 'full' },
  { path: 'profile' , component: AisProfileComponent },
  { path: 'customer' , component: CustomerComponent },
  { path: 'role' , component: RoleComponent },
  { path: 'user' , component: UserComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
