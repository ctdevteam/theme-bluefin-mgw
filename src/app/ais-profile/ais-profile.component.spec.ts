import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AisProfileComponent } from './ais-profile.component';

describe('AisProfileComponent', () => {
  let component: AisProfileComponent;
  let fixture: ComponentFixture<AisProfileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AisProfileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AisProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
