import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

  cars= [
    {"name": "Somchai", "year": 1950, "experience": "70", "role": "Admin"},
    {"name": "Audi", "year": 2011, "experience": "30", "role": "Admin"},
    {"name": "Renault", "year": 2005, "experience": "2", "role": "PO"},
    {"name": "BMW", "year": 2003, "experience": "3", "role": "DM"},
    {"name": "Mercedes", "year": 1995, "experience": "1", "role": "DM"},
]

  cols: any[];

  constructor() { }

  ngOnInit() {
      // this.carService.getCarsSmall().then(cars => this.cars = cars);

  }

}
