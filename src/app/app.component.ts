import { Input,Component, AfterViewInit, NgZone, ElementRef, Renderer, ViewChild, OnInit } from '@angular/core';

import {
  Router,// import as RouterEvent to avoid confusion with the DOM Event
  Event as RouterEvent, NavigationStart, NavigationEnd, NavigationCancel, NavigationError
} from '@angular/router';
import {MenuItem} from 'primeng/primeng';
import {trigger, state, style, transition, animate} from '@angular/animations';
enum MenuMode {
  STATIC,
  OVERLAY,
  SLIM
};
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  animations: [
    trigger('children', [
      state('hiddenAnimated', style({
        height: '0px'
      })),
      state('visibleAnimated', style({
        height: '*'
      })),
      state('visible', style({
        height: '*'
      })),
      state('hidden', style({
        height: '0px'
      })),
      transition('visibleAnimated => hiddenAnimated', animate('400ms cubic-bezier(0.86, 0, 0.07, 1)')),
      transition('hiddenAnimated => visibleAnimated', animate('400ms cubic-bezier(0.86, 0, 0.07, 1)'))
    ])
  ],
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  // title = 'bf-mgw-css';
  // items
    
  //   ngOnInit() {
  //       this.items = [
  //           {label: 'Angular.io', icon: 'fa fa-link', url: 'http://angular.io'},
  //           {label: 'Theming', icon: 'fa fa-paint-brush', routerLink: ['/theming']}
  //       ];
  //   }
  menu: MenuMode = MenuMode.STATIC;

  layout: string = 'default';

  darkMenu: boolean;

  documentClickListener: Function;

  staticMenuInactive: boolean;

  overlayMenuActive: boolean;

  mobileMenuActive: boolean;

  menuClick: boolean;

  menuButtonClick: boolean;

  topbarMenuButtonClick: boolean;

  topbarMenuClick: boolean;

  topbarMenuActive: boolean;

  activeTopbarItem: Element;

  resetSlim: boolean;

  loading: boolean = true;

  checkAuth = false;
  checkError = false;
  item
  root = true
  @Input() visible: boolean;

  @Input() disabled: boolean;

  _reset: boolean;

  activeIndex: number;

  hover: boolean;
  constructor(private router: Router,
              private ngZone: NgZone,
              private renderer: Renderer) {


    router.events.subscribe((event: RouterEvent) => {
      this._navigationInterceptor(event);
    });

    let permission;
    // if (sessionStorage.getItem('permission') && sessionStorage.getItem('userName') && sessionStorage.getItem('name') ) {
    //   permission = sessionStorage.getItem('permission');
    //   if (permission.match('[]')) {
    //     this.checkError = true;
    //   }else if (permission.match('"view":true')) {
    //     this.checkAuth = false;
    //   }else {
    //     this.checkAuth = true;
    //   }
    // }else {
    //   console.log('yes or no');
      
    //   this.request.get('/userPermiss', {}).then(response => {
    //     permission = JSON.stringify(response.resultData.permission);
    //     if (permission.match('[]')) {
    //       this.checkError = true;
    //     }else if (permission.match('"view":true')) {
    //       this.checkAuth = false;
    //     }else {
    //       this.checkAuth = true;
    //     }
    //   });
    // }
    this.checkAuth = false;
  }
  ngOnInit() {
    // this.common.getAPIPermission();

    // let permission = JSON.stringify(this.setPermission.permission);
    this.item = [
      {
          label: 'AIS Wifi Profile Manager', icon: 'fa fa-wifi', visible: true,
          items: [
              {
                  label: 'AIS Wifi Profile',
                  icon: 'fa fa-address-card',
                  routerLink: ['/profile'],
                  visible: true
              }
          ]
      },
      {
          label: 'Customer Inspect', icon: 'fa fa-users', visible: true,
          items: [
              {
                  label: 'วิเคราะห์ปัญหาลูกค้า MASS',
                  icon: 'fa fa-bar-chart',
                  routerLink: ['/customer'],
                  visible: true
              }
          ]
      },
      {
          label: 'User Management', icon: 'fa fa-fw fa-cogs', visible: true,
          items: [
              {label: 'User Management', icon: 'fa fa-user-circle-o', routerLink: ['/user'], visible: true},
              {label: 'Role Management', icon: 'fa fa-check-circle', routerLink: ['/role'], visible: true}
          ]
      },
  ];

  }

  // Shows and hides the loading spinner during RouterEvent changes
  private _navigationInterceptor(event: RouterEvent): void {

    // if (event instanceof NavigationStart) {

    //     // We wanna run this function outside of Angular's zone to
    //     // bypass change detection
    //     this.ngZone.runOutsideAngular(() => {

    //         // For simplicity we are going to turn opacity on / off
    //         // you could add/remove a class for more advanced styling
    //         // and enter/leave animation of the spinner
    //         this.renderer.setElementStyle(
    //             this.spinnerElement.nativeElement,
    //             'opacity',
    //             '1'
    //         );
    //     });
    // }
    // if (event instanceof NavigationEnd) {
    //     this._hideSpinner();
    // }

    // // Set loading state to false in both of the below events to
    // // hide the spinner in case a request fails
    // if (event instanceof NavigationCancel) {
    //     this._hideSpinner
    // }
    // if (event instanceof NavigationError) {
    //     this._hideSpinner();
    // }

    if (event instanceof NavigationStart) {
      this.loading = true;
    }
    if (event instanceof NavigationEnd) {
      this.loading = false;
    }

    // Set loading state to false in both of the below events to hide the spinner in case a request fails
    if (event instanceof NavigationCancel) {
      this.loading = false;
    }
    if (event instanceof NavigationError) {
      this.loading = false;
    }
  }

  // private _hideSpinner(): void {

  //     // We wanna run this function outside of Angular's zone to
  //     // bypass change detection,
  //     this.ngZone.runOutsideAngular(() => {

  //         // For simplicity we are going to turn opacity on / off
  //         // you could add/remove a class for more advanced styling
  //         // and enter/leave animation of the spinner
  //         this.renderer.setElementStyle(
  //             this.spinnerElement.nativeElement,
  //             'opacity',
  //             '0'
  //         );
  //     });
  // }

  ngAfterViewInit() {
    this.documentClickListener = this.renderer.listenGlobal('body', 'click', (event) => {
      if (!this.menuClick && !this.menuButtonClick) {
        this.mobileMenuActive = false;
        this.overlayMenuActive = false;
        this.resetSlim = true;
      }

      if (!this.topbarMenuClick && !this.topbarMenuButtonClick) {
        this.topbarMenuActive = false;
      }

      this.menuClick = false;
      this.menuButtonClick = false;
      this.topbarMenuClick = false;
      this.topbarMenuButtonClick = false;
    });

  }

  onMenuButtonClick(event: Event) {
    this.menuButtonClick = true;

    if (this.isMobile()) {
      this.mobileMenuActive = !this.mobileMenuActive;
    }
    else {
      if (this.staticMenu)
        this.staticMenuInactive = !this.staticMenuInactive;
      else if (this.overlayMenu)
        this.overlayMenuActive = !this.overlayMenuActive;
    }

    event.preventDefault();
  }

  onTopbarMenuButtonClick(event: Event) {
    this.topbarMenuButtonClick = true;
    this.topbarMenuActive = !this.topbarMenuActive;
    event.preventDefault();
  }

  onTopbarItemClick(event: Event, item: Element) {
    if (this.activeTopbarItem === item)
      this.activeTopbarItem = null;
    else
      this.activeTopbarItem = item;

    event.preventDefault();
  }

  onTopbarMenuClick(event: Event) {
    this.topbarMenuClick = true;
  }

  onMenuClick(event: Event) {
    this.menuClick = true;
    this.resetSlim = false;
  }

  get slimMenu(): boolean {
    return this.menu === MenuMode.SLIM;
  }

  get overlayMenu(): boolean {
    return this.menu === MenuMode.OVERLAY;
  }

  get staticMenu(): boolean {
    return this.menu === MenuMode.STATIC;
  }

  changeToSlimMenu() {
    this.menu = MenuMode.SLIM;
  }

  changeToOverlayMenu() {
    this.menu = MenuMode.OVERLAY;
  }

  changeToStaticMenu() {
    this.menu = MenuMode.STATIC;
  }

  isMobile() {
    return window.innerWidth < 640;
  }


  itemClick(event: Event, item: MenuItem, index: number) {
    //avoid processing disabled items
    if(item.disabled) {
      event.preventDefault();
      return true;
    }

    //activate current item and deactivate active sibling if any
    if(item.routerLink || item.items) {
      this.activeIndex = (this.activeIndex === index) ? null : index;
    }

    //execute command
    if(item.command) {
      item.command({
        originalEvent: event,
        item: item
      });
    }

    //prevent hash change
    if(item.items || (!item.url && !item.routerLink)) {
      event.preventDefault();
    }

    //hide menu
    if(!item.items) {
      if(this.overlayMenu || this.isMobile()) {
        this.overlayMenuActive = false;
        this.mobileMenuActive = false;
      }

      if(!this.root && this.slimMenu) {
        this.resetSlim = true;
      }
    }
  }

  isActive(index: number): boolean {
    return this.activeIndex === index;
  }

  unsubscribe(item: any) {
    if(item.eventEmitter) {
      item.eventEmitter.unsubscribe();
    }

    if(item.items) {
      for(let childItem of item.items) {
        this.unsubscribe(childItem);
      }
    }
  }

  @Input() get reset(): boolean {
    return this._reset;
  }

  set reset(val:boolean) {
    this._reset = val;

    if(this._reset && this.slimMenu) {
      this.activeIndex = null;
    }
  }

  ngOnDestroy() {
    if(this.item && this.item.items) {
      for(let item of this.item.items) {
        this.unsubscribe(item);
      }
    }
  }

  changeTheme(theme) {
    let themeLink: HTMLLinkElement = <HTMLLinkElement> document.getElementById('theme-css');
    themeLink.href = 'assets/theme/theme-' + theme + '.css';
}

changeLayout(layout) {
    this.layout = layout;
    let layoutLink: HTMLLinkElement = <HTMLLinkElement> document.getElementById('layout-css');
    layoutLink.href = 'assets/layout/css/layout-' + layout + '.css';
}
}

